<?php

namespace spec\Demo;

use Demo\Markdown;
use Demo\Reader;
use Demo\Writer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MarkdownSpec extends ObjectBehavior
{
    function let(Writer $writer)
    {
        $this->beConstructedWith($writer);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Markdown::class);
    }

    function it_converts_plain_text_to_html_paragraphs()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $this->toHtml("Hi, there!")->shouldReturn("<p>Hi, there!</p>");
    }

    /**
     * @param Reader $reader
     */
    function it_converts_text_from_an_external_source(Reader $reader)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $reader->getMarkDown()->willReturn("Hi, there");
        
        /** @noinspection PhpUndefinedMethodInspection */
        $this->toHtmlFromReader($reader)->shouldReturn("<p>Hi, there</p>");
    }

    function it_outputs_converted_text(Writer $write)
    {
        $write->writeText("<p>Hi, there</p>")->shouldBeCalled();

        /** @noinspection PhpUndefinedMethodInspection */
        $this->outputHtml("Hi, there", $write);

        $write->writeText("<p>Hi, there</p>")->shouldHaveBeenCalled();
    }

    function it_outputs_converted_text_in_constructor(Writer $writer)
    {
        $writer->writeText("<p>Hi, there</p>")->shouldBeCalled();

        /** @noinspection PhpUndefinedMethodInspection */
        $this->outputHtmlDefault("Hi, there");
    }
}
