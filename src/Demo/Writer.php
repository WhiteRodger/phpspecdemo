<?php

declare(strict_types=1);

namespace Demo;

interface Writer
{
    /**
     * @param string $text
     * @return void
     */
    public function writeText(string $text);
}
