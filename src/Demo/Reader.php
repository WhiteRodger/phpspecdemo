<?php

declare(strict_types=1);

namespace Demo;

interface Reader
{
    public function getMarkDown();
}
