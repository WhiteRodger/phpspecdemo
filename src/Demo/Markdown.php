<?php

namespace Demo;

class Markdown
{
    /**
     * @var Writer
     */
    private $writer;
    
    public function __construct($argument1)
    {
        $this->writer = $argument1;
    }

    /**
     * @param string $argument1
     * @return string
     */
    public function toHtml(string $argument1)
    {
        return "<p>" . $argument1 . "</p>";
    }

    /**
     * @param Reader $argument1
     * @return string
     */
    public function toHtmlFromReader(Reader $argument1)
    {
        return "<p>" . $argument1->getMarkDown() . "</p>";
    }

    public function outputHtml(string $text, Writer $writer)
    {
        $writer->writeText("<p>" . $text . "</p>");
    }

    public function outputHtmlDefault(string $text)
    {
        $this->writer->writeText("<p>" . $text . "</p>");
    }
}
